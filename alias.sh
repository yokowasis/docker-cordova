alias cordova="docker exec -w \${PWD} -it cordova-cont cordova"
alias jarsigner="docker exec -w \${PWD} -it cordova-cont jarsigner"
alias zipalign="docker exec -w \${PWD} -it cordova-cont zipalign"
