FROM ubuntu:18.04
LABEL author="yokowasis <yokowasis@gmail.com>"

ENV GRADLE_HOME=/opt/gradle/gradle-6.3/
ENV PATH=${GRADLE_HOME}/bin:/opt/android-sdk-linux/build-tools/29.0.3/:${PATH}
ENV ANDROID_SDK_ROOT=/opt/android-sdk-linux/

RUN apt-get update

RUN DEBIAN_FRONTEND=noninteractive apt -y install npm openjdk-8-jdk wget unzip git
RUN npm install -g cordova
RUN wget https://services.gradle.org/distributions/gradle-6.3-bin.zip -P /tmp
RUN unzip -d /opt/gradle /tmp/gradle-*.zip
RUN echo 'export GRADLE_HOME=/opt/gradle/gradle-6.3/' >> /etc/profile.d/gradle.sh
RUN echo 'export PATH=${GRADLE_HOME}/bin:/opt/android-sdk-linux/build-tools/29.0.3/:${PATH}' >> /etc/profile.d/gradle.sh
RUN echo 'export ANDROID_SDK_ROOT=/opt/android-sdk-linux/' >> /etc/profile.d/gradle.sh

